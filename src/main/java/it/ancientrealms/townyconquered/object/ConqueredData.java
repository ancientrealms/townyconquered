package it.ancientrealms.townyconquered.object;

import java.util.UUID;

public final class ConqueredData
{
    private final UUID townUUID;
    private final UUID nationUUID;
    private final int days;
    private int count;
    private final int tax;

    public ConqueredData(UUID townUUID, UUID nationUUID, int days, int count, int tax)
    {
        this.townUUID = townUUID;
        this.nationUUID = nationUUID;
        this.days = days;
        this.count = count;
        this.tax = tax;
    }

    public UUID getTownUUID()
    {
        return this.townUUID;
    }

    public UUID getNationUUID()
    {
        return this.nationUUID;
    }

    public int getDays()
    {
        return this.days;
    }

    public int getCount()
    {
        return this.count;
    }

    public int getTax()
    {
        return this.tax;
    }

    public void incrementCount()
    {
        this.count++;
    }
}
