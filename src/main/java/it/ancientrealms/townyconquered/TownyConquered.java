package it.ancientrealms.townyconquered;

import java.io.File;

import org.bukkit.plugin.java.JavaPlugin;

import com.electronwill.nightconfig.core.file.FileConfig;

import it.ancientrealms.townyconquered.command.AddCommand;
import it.ancientrealms.townyconquered.command.ReloadCommand;
import it.ancientrealms.townyconquered.command.RemoveCommand;
import it.ancientrealms.townyconquered.listener.Listener;
import it.ancientrealms.townyconquered.manager.Manager;
import it.ancientrealms.townyconquered.util.SQLite;

public final class TownyConquered extends JavaPlugin
{
    private static TownyConquered INSTANCE;
    private Manager manager;
    private FileConfig messages;

    @Override
    public void onEnable()
    {
        INSTANCE = this;

        this.saveResource("messages.yml", false);
        this.messages = FileConfig.builder(new File(this.getDataFolder(), "messages.yml"))
                .autoreload()
                .autosave()
                .build();
        this.messages.load();

        SQLite.init();

        this.manager = new Manager();
        this.manager.reload();
        this.manager.init();

        this.getServer().getPluginManager().registerEvents(new Listener(), this);

        AddCommand.register();
        ReloadCommand.register();
        RemoveCommand.register();
    }

    @Override
    public void onDisable()
    {
    }

    public static TownyConquered getInstance()
    {
        return INSTANCE;
    }

    public Manager getManager()
    {
        return this.manager;
    }

    public String getMessage(String path)
    {
        return (String) this.messages.get(path);
    }
}
