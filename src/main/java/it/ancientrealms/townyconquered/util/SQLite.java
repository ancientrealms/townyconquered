package it.ancientrealms.townyconquered.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import it.ancientrealms.townyconquered.TownyConquered;
import it.ancientrealms.townyconquered.object.ConqueredData;

public final class SQLite
{
    private static final TownyConquered plugin = TownyConquered.getInstance();
    private static Connection sqlite;

    public static void init()
    {
        try
        {
            sqlite = DriverManager.getConnection("jdbc:sqlite:" + plugin.getDataFolder().toPath().resolve("towns.db"));

            final ResultSet tables = sqlite.getMetaData().getTables(null, null, "towns", new String[] { "TABLE" });
            final Statement statement = sqlite.createStatement();

            if (!tables.next())
            {
                statement.executeUpdate("create table `towns` (\n"
                        + "    `town_id` tinytext not null,\n"
                        + "    `nation_id` tinytext NOT NULL,\n"
                        + "    `days` integer not null,\n"
                        + "    `count` integer not null,\n"
                        + "    `tax` integer not null"
                        + ")");
            }

            tables.close();
            statement.close();
        }
        catch (SQLException e)
        {
            plugin.getLogger().severe("SQLState Code: " + e.getSQLState() + ", Message: " + e.getMessage());
        }
    }

    public static void insert(ConqueredData data)
    {
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
            try
            {
                final PreparedStatement statement = sqlite.prepareStatement("insert into `towns` (town_id, nation_id, days, count, tax) VALUES(?, ?, ?, ?, ?)");
                statement.setString(1, data.getTownUUID().toString());
                statement.setString(2, data.getNationUUID().toString());
                statement.setInt(3, data.getDays());
                statement.setInt(4, data.getCount());
                statement.setInt(5, data.getTax());
                statement.execute();
                statement.close();
            }
            catch (SQLException e)
            {
                plugin.getLogger().severe("SQLState Code: " + e.getSQLState() + ", Message: " + e.getMessage());
            }
        });
    }

    public static ResultSet get()
    {
        try
        {
            final Statement statement = sqlite.createStatement();
            final ResultSet result = statement.executeQuery("select * from `towns`");
            return result;
        }
        catch (SQLException e)
        {
            plugin.getLogger().severe("SQLState Code: " + e.getSQLState() + ", Message: " + e.getMessage());
            return null;
        }
    }

    public static void remove(String townId)
    {
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
            try
            {
                final Statement statement = sqlite.createStatement();
                statement.executeUpdate("delete from `towns` where town_id = '%s'".formatted(townId));
                statement.close();
            }
            catch (SQLException e)
            {
                plugin.getLogger().severe("SQLState Code: " + e.getSQLState() + ", Message: " + e.getMessage());
            }
        });
    }

    public static void update(ConqueredData data)
    {
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
            try
            {
                final Statement statement = sqlite.createStatement();
                statement.executeUpdate("update `towns` set count = %s where town_id = '%s'".formatted(data.getCount(), data.getTownUUID().toString()));
                statement.close();
            }
            catch (SQLException e)
            {
                plugin.getLogger().severe("SQLState Code: " + e.getSQLState() + ", Message: " + e.getMessage());
            }
        });
    }
}
