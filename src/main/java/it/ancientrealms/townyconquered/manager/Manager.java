package it.ancientrealms.townyconquered.manager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.palmergames.bukkit.towny.TownyEconomyHandler;
import com.palmergames.bukkit.towny.TownyMessaging;
import com.palmergames.bukkit.towny.TownyUniverse;
import com.palmergames.bukkit.towny.object.Nation;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.metadata.StringDataField;

import it.ancientrealms.townyconquered.TownyConquered;
import it.ancientrealms.townyconquered.object.ConqueredData;
import it.ancientrealms.townyconquered.util.SQLite;

public final class Manager
{
    private final TownyConquered plugin = TownyConquered.getInstance();
    private final TownyUniverse towny = TownyUniverse.getInstance();
    private List<ConqueredData> towns = new ArrayList<>();

    public void init()
    {
        for (Town t : this.towny.getTowns())
        {
            if (t.hasMeta("occupied"))
            {
                final ConqueredData conqueredtown = this.getTown(t);

                if (conqueredtown == null)
                {
                    this.updateTown(t, null);
                }
                else
                {
                    this.updateTown(t, this.towny.getNation(conqueredtown.getNationUUID()).getName());
                }
            }
            else
            {
                this.updateTown(t, null);
            }
        }

        for (Nation n : this.towny.getNations())
        {
            this.updateNation(n);
        }
    }

    public void addConqueror(Town town, Nation nation, int days, int tax)
    {
        final ConqueredData data = new ConqueredData(town.getUUID(), nation.getUUID(), days, 0, tax);

        SQLite.insert(data);

        this.towns.add(data);
        this.updateTown(town, nation.getName());
        this.updateNation(nation);

        TownyMessaging.sendGlobalMessage(plugin.getMessage("town_conquered").formatted(town.getName(), nation.getName(), days));
    }

    public void removeTown(Town town, boolean disband)
    {
        final ConqueredData data = this.towns.stream().filter(t -> t.getTownUUID().equals(town.getUUID())).findAny().get();
        final UUID townid = town.getUUID();
        this.towns.removeIf(t -> t.getTownUUID().equals(townid));

        SQLite.remove(townid.toString());

        final Town t = this.towny.getTown(townid);

        if (disband)
        {
            this.towny.getDataSource().removeTown(t);
            return;
        }

        TownyMessaging.sendGlobalMessage(plugin.getMessage("town_unconquered").formatted(t.getName(), t.getMetadata("occupied").getValue()));

        this.updateTown(t, null);
        t.save();

        this.updateNation(this.towny.getNation(data.getNationUUID()));
    }

    public ConqueredData getTown(Town town)
    {
        return this.towns.stream().filter(t -> t.getTownUUID().equals(town.getUUID())).findAny().orElse(null);
    }

    public List<ConqueredData> getListTowns()
    {
        return this.towns;
    }

    public void updateTown(Town town, String nationName)
    {
        final ConqueredData data = this.getTown(town);

        final String occupiedlabel = plugin.getMessage("town_occupied_label");
        final String remainingdayslabel = plugin.getMessage("town_occupied_days_label");
        final String taxamountlabel = plugin.getMessage("town_occupied_tax_label");

        final String days = data != null ? String.valueOf(Integer.valueOf(data.getDays()) - data.getCount()) : null;
        final String tax = data != null ? TownyEconomyHandler.getFormattedBalance(Double.valueOf(data.getTax())) : null;

        final StringDataField occupied = new StringDataField("occupied", nationName, occupiedlabel);
        final StringDataField occupieddays = new StringDataField("occupiedDays", days, remainingdayslabel);
        final StringDataField occupiedtaxamount = new StringDataField("occupiedTaxAmount", tax, taxamountlabel);

        if (data == null)
        {
            occupied.setValue(plugin.getMessage("town_unoccupied_label"));

            if (town.hasMeta("occupiedDays"))
            {
                town.removeMetaData(occupieddays, true);
                town.removeMetaData(occupiedtaxamount, true);
            }
        }
        else
        {
            town.addMetaData(occupieddays, true);
            town.addMetaData(occupiedtaxamount, true);
        }

        town.addMetaData(occupied, true);
    }

    public void updateNation(Nation nation)
    {
        final String conqueredlabel = plugin.getMessage("nation_conquered_label");
        final List<Town> conqueredtowns = this.towns.stream()
                .filter(t -> t.getNationUUID().equals(nation.getUUID()))
                .map(t -> this.towny.getTown(t.getTownUUID()))
                .toList();
        final String occupiedvalue = conqueredtowns.isEmpty()
                ? plugin.getMessage("nation_zero_conquered")
                : String.join(", ", conqueredtowns.stream().map(t -> t.getName()).toList());
        final StringDataField conqueredfield = new StringDataField("nationConqueredTowns", occupiedvalue, conqueredlabel);

        final String earningslabel = plugin.getMessage("nation_earnings_from_conquests");
        final String earningsvalue = conqueredtowns.stream()
                .map(t -> Double.valueOf(this.getTown(t).getTax()))
                .reduce(Double::sum).orElse(0.0)
                .toString();
        final String fmtearningsvalue = TownyEconomyHandler.getFormattedBalance(Double.valueOf(earningsvalue));
        final StringDataField earningsfield = new StringDataField("nationEarnings", fmtearningsvalue, earningslabel);

        nation.addMetaData(conqueredfield);
        nation.addMetaData(earningsfield);
    }

    public void reload()
    {
        this.towns.clear();

        try
        {
            final ResultSet data = SQLite.get();

            while (data.next())
            {
                final UUID townid = UUID.fromString(data.getString("town_id"));
                final UUID nationid = UUID.fromString(data.getString("nation_id"));
                final int days = data.getInt("days");
                final int count = data.getInt("count");
                final int tax = data.getInt("tax");

                this.towns.add(new ConqueredData(townid, nationid, days, count, tax));
                this.updateTown(this.towny.getTown(townid), this.towny.getNation(nationid).getName());
                this.updateNation(this.towny.getNation(nationid));
            }

            data.close();
        }
        catch (SQLException e)
        {
            plugin.getLogger().severe("SQLState Code: " + e.getSQLState() + ", Message: " + e.getMessage());
        }
    }
}
