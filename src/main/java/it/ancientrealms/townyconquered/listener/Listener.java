package it.ancientrealms.townyconquered.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.event.EventHandler;

import com.palmergames.bukkit.towny.TownyEconomyHandler;
import com.palmergames.bukkit.towny.TownyMessaging;
import com.palmergames.bukkit.towny.TownySettings;
import com.palmergames.bukkit.towny.TownyUniverse;
import com.palmergames.bukkit.towny.event.DeleteNationEvent;
import com.palmergames.bukkit.towny.event.NewNationEvent;
import com.palmergames.bukkit.towny.event.NewTownEvent;
import com.palmergames.bukkit.towny.event.PreNewDayEvent;
import com.palmergames.bukkit.towny.object.Nation;
import com.palmergames.bukkit.towny.object.Town;

import it.ancientrealms.townyconquered.TownyConquered;
import it.ancientrealms.townyconquered.object.ConqueredData;
import it.ancientrealms.townyconquered.util.SQLite;

public final class Listener implements org.bukkit.event.Listener
{
    private final TownyConquered plugin = TownyConquered.getInstance();
    private final TownyUniverse towny = TownyUniverse.getInstance();

    @EventHandler
    public void preNewDay(PreNewDayEvent event)
    {
        final Map<Town, Boolean> remove = new HashMap<>();

        for (ConqueredData data : plugin.getManager().getListTowns())
        {
            final Town town = this.towny.getTown(data.getTownUUID());
            final Nation nation = this.towny.getNation(data.getNationUUID());
            final String nationname = nation.getName();
            final double tax = Double.valueOf(data.getTax());
            final String reason = plugin.getMessage("tax_payment_to").formatted(nationname);
            final String fmtbalance = TownyEconomyHandler.getFormattedBalance(tax);

            if (town.getAccount().canPayFromHoldings(tax))
            {
                town.getAccount().payTo(tax, nation, reason);
                TownyMessaging.sendPrefixedTownMessage(town,
                        plugin.getMessage("payed_tax").formatted(fmtbalance));
            }
            else
            {
                if (!TownySettings.isTownBankruptcyEnabled())
                {
                    TownyMessaging.sendPrefixedTownMessage(town, plugin.getMessage("town_couldnt_pay_tax").formatted(fmtbalance));
                    remove.put(town, true);
                    continue;
                }

                if (town.getAccount().getHoldingBalance() - tax < town.getAccount().getDebtCap() * -1)
                {
                    if (TownySettings.isUpkeepDeletingTownsThatReachDebtCap())
                    {
                        TownyMessaging.sendPrefixedTownMessage(town, plugin.getMessage("town_couldnt_pay_tax").formatted(fmtbalance));
                        remove.put(town, true);
                        continue;
                    }
                }

                town.getAccount().withdraw(tax, reason);
                nation.getAccount().deposit(tax, plugin.getMessage("payed_tax").formatted(town.getName(), fmtbalance));
                TownyMessaging.sendPrefixedTownMessage(town, plugin.getMessage("payed_tax").formatted(fmtbalance));
            }

            data.incrementCount();
            plugin.getManager().updateTown(town, nationname);

            final int count = data.getCount();
            final int days = Integer.valueOf(data.getDays());

            if (count == days)
            {
                remove.put(town, false);
            }
            else
            {
                SQLite.update(data);
            }
        }

        for (Map.Entry<Town, Boolean> entry : remove.entrySet())
        {
            final Town town = entry.getKey();
            final boolean disband = entry.getValue();

            plugin.getManager().removeTown(town, disband);
        }
    }

    @EventHandler
    public void newTown(NewTownEvent event)
    {
        plugin.getManager().updateTown(event.getTown(), null);
    }

    @EventHandler
    public void newNation(NewNationEvent event)
    {
        plugin.getManager().updateNation(event.getNation());
    }

    @EventHandler
    public void deleteNation(DeleteNationEvent event)
    {
        final List<ConqueredData> remove = new ArrayList<>();

        for (ConqueredData data : plugin.getManager().getListTowns())
        {
            if (data.getNationUUID().equals(event.getNationUUID()))
            {
                remove.add(data);
            }
        }

        for (ConqueredData data : remove)
        {
            plugin.getManager().removeTown(this.towny.getTown(data.getTownUUID()), false);
        }
    }
}
