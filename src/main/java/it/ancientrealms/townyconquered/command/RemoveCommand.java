package it.ancientrealms.townyconquered.command;

import com.palmergames.bukkit.towny.TownyMessaging;
import com.palmergames.bukkit.towny.TownyUniverse;
import com.palmergames.bukkit.towny.object.Town;

import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.LiteralArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import it.ancientrealms.townyconquered.TownyConquered;

public final class RemoveCommand
{
    private static final TownyConquered plugin = TownyConquered.getInstance();
    private static final TownyUniverse townyuniverse = TownyUniverse.getInstance();

    public static void register()
    {
        new CommandAPICommand("townyconquered")
                .withPermission("townyconquered.command.remove")
                .withArguments(new LiteralArgument("remove"))
                .withArguments(new StringArgument("town").replaceSuggestions(info -> {
                    return plugin.getManager().getListTowns().stream().map(t -> townyuniverse.getTown(t.getTownUUID()).getName())
                            .toArray(String[]::new);
                }))
                .executes((sender, args) -> {
                    final String tname = (String) args[0];
                    final Town town = townyuniverse.getTown(tname);

                    if (town == null)
                    {
                        TownyMessaging.sendErrorMsg(sender, plugin.getMessage("town_not_found").formatted(tname));
                        return;
                    }

                    if (plugin.getManager().getTown(town) == null)
                    {
                        TownyMessaging.sendErrorMsg(sender, plugin.getMessage("town_not_conquered").formatted(tname));
                        return;
                    }

                    plugin.getManager().removeTown(town, false);
                })
                .register();
    }
}
