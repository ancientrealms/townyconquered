package it.ancientrealms.townyconquered.command;

import com.palmergames.bukkit.towny.TownyMessaging;
import com.palmergames.bukkit.towny.TownyUniverse;
import com.palmergames.bukkit.towny.object.Nation;
import com.palmergames.bukkit.towny.object.Town;

import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.IntegerArgument;
import dev.jorel.commandapi.arguments.LiteralArgument;
import dev.jorel.commandapi.arguments.StringArgument;
import it.ancientrealms.townyconquered.TownyConquered;

public final class AddCommand
{
    private static final TownyConquered plugin = TownyConquered.getInstance();
    private static final TownyUniverse townyuniverse = TownyUniverse.getInstance();

    public static void register()
    {
        new CommandAPICommand("townyconquered")
                .withPermission("townyconquered.command.add")
                .withArguments(new LiteralArgument("add"))
                .withArguments(new StringArgument("townName").replaceSuggestions(info -> {
                    return townyuniverse.getTowns().stream().map(t -> t.getName()).toArray(String[]::new);
                }))
                .withArguments(new StringArgument("nationName").replaceSuggestions(info -> {
                    return townyuniverse.getNations().stream().map(n -> n.getName()).toArray(String[]::new);
                }))
                .withArguments(new IntegerArgument("days").replaceSuggestions(info -> {
                    return new String[] { "1", "30", "90", "365" };
                }))
                .withArguments(new IntegerArgument("tax").replaceSuggestions(info -> {
                    return new String[] { "1", "10", "100", "1000" };
                }))
                .executes((sender, args) -> {
                    final String tname = (String) args[0];
                    final String nname = (String) args[1];
                    final int days = (int) args[2];
                    final int tax = (int) args[3];

                    final Town town = townyuniverse.getTown(tname);
                    final Nation nation = townyuniverse.getNation(nname);

                    if (town == null)
                    {
                        TownyMessaging.sendErrorMsg(sender, plugin.getMessage("town_not_found").formatted(tname));
                        return;
                    }

                    if (plugin.getManager().getTown(town) != null)
                    {
                        TownyMessaging.sendErrorMsg(sender, plugin.getMessage("town_already_conquered").formatted(tname));
                        return;
                    }

                    if (nation == null)
                    {
                        TownyMessaging.sendErrorMsg(sender, plugin.getMessage("nation_not_found").formatted(nname));
                        return;
                    }

                    if (nation.hasTown(town))
                    {
                        TownyMessaging.sendErrorMsg(sender, plugin.getMessage("nation_cannot_conquer_own_town").formatted(tname));
                        return;
                    }

                    plugin.getManager().addConqueror(town, nation, days, tax);
                })
                .register();
    }
}
