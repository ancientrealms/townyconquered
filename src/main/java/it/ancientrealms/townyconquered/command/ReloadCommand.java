package it.ancientrealms.townyconquered.command;

import com.palmergames.bukkit.towny.TownyMessaging;

import dev.jorel.commandapi.CommandAPICommand;
import dev.jorel.commandapi.arguments.LiteralArgument;
import it.ancientrealms.townyconquered.TownyConquered;

public final class ReloadCommand
{
    private static final TownyConquered plugin = TownyConquered.getInstance();

    public static void register()
    {
        new CommandAPICommand("townyconquered")
                .withPermission("townyconquered.command.reload")
                .withArguments(new LiteralArgument("reload"))
                .executes((sender, args) -> {
                    plugin.getManager().reload();
                    TownyMessaging.sendMsg(sender, plugin.getMessage("reload_success"));
                })
                .register();
    }
}
